package com.psybergate.vac201912.taxHandling ;
import java.time.Year;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.psybergate.vac201912.taxHandling.TaxRecord;

public class TaxPayer
{
	public TaxPayer( int taxNumber , String name , String surname , int yearOfBirth )
	{
		this.name = name ;
		this.surname = surname ;
		this.yearOfBirth = yearOfBirth ;
		this.taxNumber = taxNumber ;
		this.taxRecords = new HashMap<Integer,TaxRecord>() ;
	}

	public int getYearOfBirth()
	{
		return this.yearOfBirth ;
	}
	public int getTaxNumber()
	{
		return this.taxNumber ;
	}
	public int getAge()
	{
		return Year.now().getValue() - this.yearOfBirth;
	}

	public String getName()
	{
		return this.name ;
	}
	public String getSurname()
	{
		return this.surname ;
	}
	public TaxRecord getTaxRecord( int year )
	{
		return taxRecords.get(year) ;
	}
	
	public Collection<?> getAllTaxRecords()
	{
		return taxRecords.values() ;
	}

	public void addTaxRecord( int year , double salary , double interest , double benefits )
	{
		TaxRecord taxRecord = new TaxRecord(getTaxNumber(),getAge(),year,salary,interest,benefits) ;
		taxRecords.put(year,taxRecord) ;
	}

	public int taxStatus( int year )
	{
		if ( taxRecords.containsKey(year))
			return TaxStatus.CALCULATED ;
		else if ( year == Year.now().getValue())
				return TaxStatus.PENDING ;
		
		return TaxStatus.NOT_AVAILABLE ;
	}

	private int yearOfBirth;
	private String name ;
	private String surname ;
	private int taxNumber ;
	//keeps tax records referenced by year
	private Map<Integer,TaxRecord> taxRecords ;
}

interface TaxStatus
{
	public static int CALCULATED = 0 ;
	public static int PENDING = 1 ;
	public static int NOT_AVAILABLE = 3 ;
}
