<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="false"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Tax Record Information</title>
		<link href="styles/display.css" rel="stylesheet">
	</head>
	<body>
	
	<div class='bgRecordDetails'>
		<form action = 'new_tax_record' method='post'>
			<label for='year'>Tax Record year</label> 
			<input class='inBoxDetails' id='year' type="number" name="year" placeholder='Year for which tax record is added..'><br>
			
			<label for='salary'>Salary</label>
			<input class='inBoxDetails' id='salary' type="number" name="salary" placeholder='Amount of money earned per annum...'><br>
			
			<label for='benefits'>Benefits</label>
			<input class='inBoxDetails' id='benefits' type="number" name="benefits" placeholder='Retirement annuity,Travel Allowance,etc...'><br>
			
			<label for='interest'>Interest</label>
			<input class='inBoxDetails' id='interest' type="number" name="interest" placeholder='Total interest earned on investments...'><br><br>
			
			<input class='rightAlign btnRecordDetails' type="submit" value="Send">
		</form>
		</div>
	</body>
</html>