package com.psybergate.vac201912.taxHandling;

/**
 * Hello world!
 *
 */
public class TaxCalculator 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
    
    /** Convert amount to salary(i.e per annum )
     * @param chosenPaymentPeriod the payment period selected by the user
     * @param TODO length of payment period
     * @param remuneration amount of money entered by the user
     * @return amount of money earned per year
     */
    public double convertToSalary( int chosenPaymentPeriod , double remuneration )
    {
    	switch ( chosenPaymentPeriod )
    	{
    	case PaymentPeriod.DAILY :
    		return remuneration * 365 ;
    	case PaymentPeriod.WEEKLY :
    		return remuneration * 52 ;
    	case PaymentPeriod.FORTNIGHTLY :
    		return remuneration * 26 ;
    	case PaymentPeriod.MONTHLY :
    		return remuneration * 12 ;
    	default : //YEARLY
    		return remuneration ;
    	}
    }
    
    /**Returns the amount allowed to count towards benefits
     * @param benefits - total amount of benefits earned e.g retirement annuity , pension fund , etc
     * @param salary
     * @return amount allowable which counts towards tax benefits
     */
    public static double getAllowableBenefits( double benefits , double salary )
    {
    	if ( benefits > TaxMacros.MaxAllowableBenefits )
    		return TaxMacros.MaxAllowableBenefits ;
    	else
    		return benefits > salary*TaxMacros.PercOfSalaryAllowableAsBenefits ? salary*TaxMacros.PercOfSalaryAllowableAsBenefits : benefits ;
    }
    /** From salary, calculate taxable income by removing benefits and interest
     * @param Salary the employee's salary
     * @param Interest the amount of money gained in interests by the user from any other investments
     * @param Benefits allowable amount counting towards tax benefits
     * @param TODO age dependency
     * @return taxable income
     */
    public static double getTaxableIncome(double salary , double benefits , double interest )
    {
    	return salary - benefits + ( interest > TaxMacros.InterestExemption ? interest - TaxMacros.InterestExemption : interest ) ;
    }

    /** Calculates tax payable from taxable income
     * @param taxableIncome amount of money to lookup in tax table
     * @param TODO age
     * @return amount of money owed in taxes as per tax table
     */
    public static double getTaxPayable(double taxableIncome )
    {
    	//TODO query a tax table database
    	if ( taxableIncome <= 79000 )
    	{
    		//tax threshold
    		return 0 ;
    	}
    	else if ( taxableIncome <= 195850 )
    	{
    		return taxableIncome * .18 ;
    	}else if ( taxableIncome <= 305850 )
    	{
    		return 35253 + .26 * ( taxableIncome - 195851 ) ;
    	}else if ( taxableIncome <= 423300 )
    	{
    		return 63853 + .31 * ( taxableIncome - 305851 ) ;
    	}else if ( taxableIncome <= 555600 )
    	{
    		return 100263 + .36 * ( taxableIncome - 423301 ) ;
    	}else if ( taxableIncome <= 708310 )
    	{
    		return 147891 + .39 * ( taxableIncome - 555601 ) ;
    	}else if ( taxableIncome <= 1500000 )
    	{
    		return 207448 + .41 * ( taxableIncome - 708311 ) ;
    	}else
    	{
    		return 532041 + .45 * ( taxableIncome - 1500001 ) ;
    	}
    }
    
    /** Calculate final tax owed to the tax-man
     * @param taxPayable
     * @return amount of money owed after rebates
     */
    public static double getFinalTaxOwed( double taxPayable , double rebateAmount )
    {
    	return taxPayable > rebateAmount ? taxPayable - rebateAmount : 0 ;
    }
    
    /** Calculates the rebate amount depending on age of tax payer
     * @param Age of the tax payer
     * @return amount of money in tax rebate
     */
    public static double getRebate( int age )
    {
    	if ( age < 65 )
    		return 14220.0 ;
    	else if ( age <= 75 )
    		return getRebate(60) + 7794.0 ;
    	else
    		return getRebate(70) + 2601.0 ;
    }
}