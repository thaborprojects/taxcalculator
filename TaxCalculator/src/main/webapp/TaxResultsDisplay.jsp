<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="false"%>
    <!-- Tag library import statements
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    <@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
    <@ taglib prefix = "sql" uri = "http://java.sun.com/jsp/jstl/sql" %>
    -->
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
    <%@page import="com.psybergate.vac201912.taxHandling.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tax Results</title>

<link href="styles/display.css" rel="stylesheet">

</head>
<body>
	<script type="text/javascript">
		function onYearChange( selector )
		{
			var salary = document.getElementById('salary');
			var taxToPay = document.getElementById('taxToPay');
			var benefits = document.getElementById('benefits');
			var interest = document.getElementById('interest');
			var taxBenefits = document.getElementById('taxBenefits');
			var taxRebate = document.getElementById('taxRebate');
			var taxableIncome = document.getElementById('taxableIncome');
			var taxPayable = document.getElementById('taxPayable');
			var taxToPay = document.getElementById('taxToPay');
			var taxNumber = document.getElementById('taxNumber');
			var age = document.getElementById('age');
			
			var taxPayerTaxNumber = taxNumber.innerText ;
			//the selected tax record year
			var year = selector.value ;
			//contact server and get the tax record
			salary.innerText = "changed this" ;
			
		}
	</script>
	<!-- Tax payer details -->
	<div>
		<fieldset class="noBorder">
			<label class='leftAlign'>Name : ${taxPayer.name}</label><br>
			<label class='leftAlign'>Surname : ${taxPayer.surname}</label><br>
			<label class='leftAlign'>Year of Birth : ${taxPayer.yearOfBirth}</label><br>
			<label class='leftAlign' for='taxNumber'>Tax Number : </label><label class='leftAlign' id='taxNumber'>${taxPayer.taxNumber}</label>
		</fieldset>
	</div>
	<!-- Display given tax record else display latest and put expandable selector aside -->
	<div>
		<fieldset>
			<legend><h4>Tax Record</h4></legend>
			<p class="rightAlign" >
				<label for="taxRecordYearsList">Tax Record Year</label>
				<c:if test="${fn:length(taxRecordYears) gt 0}">
					<select id="taxRecordYearsList" onChange='onYearChange(this)'>
						<c:forEach items="${taxRecordYears}" var="year">
							<option id='${year}' value="${year}">${year}</option>
						</c:forEach>
					</select>
				</c:if>
			</p>
		</fieldset>
		
		<fieldset>
			<legend>Tax Record Summary</legend>
			<label class='valueLabelLabel'>Salary : </label>
			<label id='salary' class='valueLabel'></label>
			<br>
			<label class='valueLabelLabel'>Tax To Pay : </label>
			<label id='taxToPay' class='valueLabel'></label>
		</fieldset>
		
		<fieldset>
			<legend> Stats for Tax Nerds </legend>
			<label class="leftAlign">Age of Tax Payer For This Record :</label>
			<label id='age'></label>
			<br>
			<br>
			<label class='valueLabelLabel'>Salary : </label>
			<label id='salary' class='valueLabel'></label>
			<label class='valueLabelLabel'>Benefits : </label>
			<label id='benefits' class='valueLabel'></label>
			<br>
			<label class='valueLabelLabel'>Interest : </label>
			<label id='interest' class='valueLabel'></label>
			<br>
			
			
			<label class='valueLabelLabel'>Tax Benefits : </label>
			<label id='taxBenefits' class='valueLabel'></label>
			<br>
			<label class='valueLabelLabel'>Tax Rebate : </label>
			<label id='taxRebate' class='valueLabel'></label>
			<br>
			<label class='valueLabelLabel'>Taxable Income :</label>
			<label id='taxableIncome' class='valueLabel'></label>
			<br>
			<label class='valueLabelLabel'>Tax payable : </label>
			<label id='taxPayable' class='valueLabel'></label>
			<br>
			<label class='valueLabelLabel'>Final Tax Owed</label>
			<label id='taxToPay' class='valueLabel'></label>
		</fieldset>
	</div>
</body>
</html>