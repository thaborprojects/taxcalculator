package com.psybergate.vac201912.uiCoordinationServlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.psybergate.vac201912.dbHandling.DatabaseHandler;
import com.psybergate.vac201912.taxHandling.TaxPayer;
import com.psybergate.vac201912.taxHandling.TaxRecord;

public class ViewAllRecords extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// contact db and route to display all records with most recently added in view
	public void doPost(HttpServletRequest req , HttpServletResponse res ) throws ServletException, IOException
	{
		//get user tax number from session
		HttpSession session = req.getSession(false) ;
		TaxPayer taxPayer ;
		
		try
		{
			if ( session == null )
			{
				//coming from start page, get parameters
				int taxNumber = Integer.parseInt(req.getParameter("taxNumber")) ;
				taxPayer = DatabaseHandler.getTaxPayer( taxNumber ) ;
				
				//add session and taxPayer
				session = req.getSession(true) ;
				session.setAttribute("taxPayer", taxPayer );
				req.setAttribute("taxPayer", taxPayer );
			}else {
				taxPayer = (TaxPayer)session.getAttribute("taxPayer") ;
			}
				

			//get all records from db
			Map<Integer,TaxRecord> taxRecords = DatabaseHandler.getAllTaxRecords(taxPayer.getTaxNumber()) ;
			//add all records to session object
			session.setAttribute("taxRecords", taxRecords);
			req.setAttribute("taxRecords", taxRecords);
			
			//******
			System.out.println("===================");
			System.out.println(taxRecords) ;
			
			//set tax record years list
			Collection<Integer> years = taxRecords.keySet() ;
			session.setAttribute("taxRecordYears", years );
			req.setAttribute("taxRecordYears", years );

			//forward to display page
			RequestDispatcher rd = req.getRequestDispatcher("TaxResultsDisplay.jsp") ;
			rd.forward(req, res);
		}catch ( SQLException e )
		{
			RequestDispatcher rd = req.getRequestDispatcher("errorPage.jsp") ;
			e.printStackTrace();
			rd.forward(req, res);
		}
	}
}
