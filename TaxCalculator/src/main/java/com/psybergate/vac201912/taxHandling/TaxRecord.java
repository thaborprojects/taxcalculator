package com.psybergate.vac201912.taxHandling;

public class TaxRecord {
	private int taxNumber;
	
	private int year;
	
	private double salary;
	
	private double interest;
	
	private double benefits;
	//after calcs
	private double taxableIncome;
	
	private double taxPayable;
	
	private double finalTaxPayable;
	
	private double taxBenefits;
	
	private double rebateAmount;
	
	private int taxPayerAge ;

	// this class will use TaxCalculator to create a tax record
	public TaxRecord(int taxNumber, int age, int year, double salary, double interest, double benefits) {
		this.taxNumber = taxNumber;
		this.year = year;
		this.salary = salary;
		this.interest = interest;
		this.benefits = benefits;

		this.taxPayerAge = age ;
		this.rebateAmount = TaxCalculator.getRebate(age);
		this.taxBenefits = TaxCalculator.getAllowableBenefits(benefits, salary);
		this.taxableIncome = TaxCalculator.getTaxableIncome(this.salary, this.taxBenefits, this.interest);
		this.taxPayable = TaxCalculator.getTaxPayable(this.taxableIncome);
		this.finalTaxPayable = TaxCalculator.getFinalTaxOwed(this.taxPayable, this.rebateAmount);
	}
	
	public int getAge()
	{
		return this.taxPayerAge ;
	}
	
	public double getFinalTaxPayable() {
		return finalTaxPayable;
	}

	public double getTaxPayable() {
		return taxPayable;
	}

	public double getTaxableIncome() {
		return taxableIncome;
	}

	public int getTaxNumber() {
		return taxNumber;
	}

	public int getYear() {
		return year;
	}

	public double getSalary() {
		return salary;
	}

	public double getInterest() {
		return interest;
	}

	public double getBenefits() {
		return benefits;
	}

	public double getTaxBenefits() {
		return taxBenefits;
	}

	public double getTaxRebate() {
		return this.rebateAmount;
	}

}
