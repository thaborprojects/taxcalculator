package com.psybergate.vac201912.taxHandling;
import static org.junit.Assert.*;
import org.junit.*;

public class TaxRecordTest {

	TaxRecord taxRecord ;
	@Before
	public void setUp()
	{
		taxRecord = new TaxRecord(123456,60,2019,500000.0,150000.0,70000) ;
	}
	@After
	public void tearDown()
	{
		taxRecord = null ;
	}
	
	@Test
	public void getBenefitsTest()
	{
		assertEquals(70000,taxRecord.getBenefits(),10E-3) ;
	}
	
	@Test
	public void getInterestTest()
	{
		assertEquals(150000,taxRecord.getInterest(),10E-3) ;
	}
	
	@Test
	public void getSalary()
	{
		assertEquals(500000.0,taxRecord.getSalary(),1E-3) ;
	}
	
	@Test
	public void getYear()
	{
		assertEquals(2019,taxRecord.getYear()) ;
	}
	
	@Test
	public void getTaxNumberTest()
	{
		assertEquals(123456,taxRecord.getTaxNumber()) ;
	}
	
	@Test
	public void getTaxableIncomeTest()
	{
		double salary = 500000.0 , benefits = 70000.0 , interest = 150000.0 ;
		double expectedTaxableIncome = salary + interest-TaxMacros.InterestExemption - benefits ;
		
		assertEquals(expectedTaxableIncome , taxRecord.getTaxableIncome() ,10E-3) ;
	}
	
	@Test
	public void getTaxPayableTest()
	{
		double expectedTaxPayable = 148124.61 ;
		assertEquals(expectedTaxPayable , taxRecord.getTaxPayable(),10E-3) ;
	}
	
	@Test
	public void getFinalTaxPayableTest()
	{
		//for under 65s
		taxRecord = new TaxRecord(123456,60,2019,500000.0,150000.0,70000) ;
		double expectedFinalTaxPayable = 148124.61 - 14220.0 ;
		
		assertEquals(expectedFinalTaxPayable,taxRecord.getFinalTaxPayable(),10E-3) ;
		
		//for between 65-75
		taxRecord = new TaxRecord(123456,70,2019,500000.0,150000.0,70000) ;
		expectedFinalTaxPayable = expectedFinalTaxPayable - 7794.0 ;
		assertEquals(expectedFinalTaxPayable,taxRecord.getFinalTaxPayable(),10E-3) ;
		
		//for over 75
		taxRecord = new TaxRecord(123456,80,2019,500000.0,150000.0,70000) ;
		expectedFinalTaxPayable = expectedFinalTaxPayable - 2601.0 ;
		assertEquals(expectedFinalTaxPayable,taxRecord.getFinalTaxPayable(),10E-3) ;
	}
	
	public void getRebateAmountTest()
	{
		//for an individual under 65
		taxRecord = new TaxRecord(0,60,0,0.0,0.0,0.0) ;
		double expectedRebate = 14220.0 ;
		assertEquals(expectedRebate,taxRecord.getTaxRebate(),10E-3) ;
		
		//for an individual between 65-75 year of age
		taxRecord = new TaxRecord(0,65,0,0.0,0.0,0.0) ;
		expectedRebate = expectedRebate +7794.0 ;
		assertEquals(expectedRebate,taxRecord.getTaxRebate(),10E-3);
		
		taxRecord = new TaxRecord(0,75,0,0.0,0.0,0.0) ;
		assertEquals(expectedRebate,taxRecord.getTaxRebate(),10E-3) ;
		
		//for an individual above 80 years
		taxRecord = new TaxRecord(0,80,0,0.0,0.0,0.0) ;
		expectedRebate = expectedRebate + 2601.0 ;
		assertEquals(expectedRebate,taxRecord.getTaxRebate(),10E-3);
	}
}
