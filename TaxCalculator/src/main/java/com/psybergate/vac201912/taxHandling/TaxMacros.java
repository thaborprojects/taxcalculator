package com.psybergate.vac201912.taxHandling;

public class TaxMacros
{
	//class for defining values related to tax
	public static final double MaxAllowableBenefits = 350000.0 ;
	public static final double PercOfSalaryAllowableAsBenefits = .275 ;
	public static final double InterestExemption = 23800 ;
	public static final double RebateAmount = 5000 ;
}
