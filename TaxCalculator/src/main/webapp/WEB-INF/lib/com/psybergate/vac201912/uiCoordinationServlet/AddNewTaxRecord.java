package com.psybergate.vac201912.uiCoordinationServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import com.psybergate.vac201912.dbHandling.DatabaseHandler;
import com.psybergate.vac201912.taxHandling.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddNewTaxRecord extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		// get taxNumber, age , year , salary , interest and benefits
		int taxNumber = 0, age = 0, year = 0;
		double salary = 0.0, interest = 0.0, benefits = 0.0;

		// get taxPayer from existing session object
		HttpSession session = req.getSession(false);

		TaxPayer taxPayer = (TaxPayer) session.getAttribute("taxPayer");
		taxNumber = taxPayer.getTaxNumber();
		age = taxPayer.getAge();

		year = Integer.parseInt(req.getParameter("year"));
		salary = Integer.parseInt(req.getParameter("salary"));
		interest = Integer.parseInt(req.getParameter("interest"));
		benefits = Integer.parseInt(req.getParameter("benefits"));

		// TODO add tax record to taxPayer and db
		// create tax record
		TaxRecord taxRecord = new TaxRecord(taxNumber, age, year, salary, interest, benefits);
		
		try
		{
		//adding record to db
		DatabaseHandler.addTaxRecord(taxRecord) ;
		
		//TODO query db for available tax record years
		
		
		// add taxRecord to session
		session.setAttribute("taxRecord", taxRecord);
		//req.setAttribute("taxRecord", taxRecord);

		// forward to results page
		RequestDispatcher rd = req.getRequestDispatcher("TaxResultsDisplay.jsp");
		rd.forward(req, res);
		}catch ( SQLException e )
		{
			RequestDispatcher rd = req.getRequestDispatcher("errorPage.jsp") ;
			e.printStackTrace();
			rd.forward(req, res);
		}
	}

	public static void log(HttpServletResponse res, String text) throws IOException {
		res.setContentType("text/html");
		PrintWriter pr = res.getWriter();
		pr.println("<html><head></head>");
		pr.println("<body>" + text + "</body></html>");
	}

}
