<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="true"%>
    <%@ page import="com.psybergate.vac201912.taxHandling.*" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>User Information</title>
	<link href="styles/display.css" rel="stylesheet">
</head>
<body>
 	<!--  TODO 1. get user availability in database by searching for availability of taxNumber during input validation 
 	      2. Create session
 	      3. Input validation -->
 	<fieldset>
 		//header of the doc, posssibly app image
 	</fieldset>

	<div class='bgUserDetails'>
		<form class='centerAlign' method='post'>
			<label for='taxNumber'>Tax Number</label>
			<input class='inBoxDetails' type='number' id='taxNumber' name='taxNumber' placeholder='Your SARS tax number...' required ><br>
			
			<!-- make the following available depending on whether tax number already exists -->
			<label for='name'>Name</label>
			<input class='inBoxDetails' type='text' id='name' name='name' placeholder='Your first name...'><br>
			
			<label for='surname'>Surname</label>
			 <input class='inBoxDetails' type='text' id='surname' name='surname' placeholder='Your last name...'><br>
			
			<label for='yearOfBirth'>Year Of Birth</label>
			<input class='inBoxDetails' type='number' id='yearOfBirth' placeholder= 'Year in which you were born...' name='yearOfBirth'><br>
			
			<!-- TODO make buttons available depending on whether taxNumber exists in DB -->
			<input class='rightAlign btnUserDetails' type='submit' formaction='registration'
				value='Register New User'>
			<input class='rightAlign btnUserDetails' type='submit' formaction='tax_records' value='View Tax Records'>
			<input class='rightAlign btnUserDetails' type='submit' formaction='new_tax_record' value='Add Tax Record'>
		</form>
	</div>
</body>
</html>