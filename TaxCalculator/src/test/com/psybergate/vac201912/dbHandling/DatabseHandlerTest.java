package com.psybergate.vac201912.dbHandling;
import org.junit.* ;
import java.sql.*;

public class DatabseHandlerTest
{
	DatabaseHandler handler ;
	@BeforeClass
	public void setUp()
	{
		handler = new DatabaseHandler() ;
	}
	
	@Test(expected=SQLException.class)
	public void connectToDatabaseErrorTest() throws SQLException
	{
		//testing whether connecting to postgres db with an invalid db name returns error
		handler.connectToDatabase("nonExistantDb");
	}
}
