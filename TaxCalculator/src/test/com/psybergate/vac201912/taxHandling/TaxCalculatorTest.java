package com.psybergate.vac201912.taxHandling;

import java.util.Random;
import junit.framework.TestCase;

/**
 * Unit tests for simple TaxCalulator. Uses JUnit 3
 */
public class TaxCalculatorTest extends TestCase
{
	//we'll using this to automate test cases
	Random rj = new Random() ;
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TaxCalculatorTest( String testName )
    {
        super( testName );
    }

    /**
     * Testing convertToSalary( int chosenPaymentPeriod , double remuneration )
     */
    public void testToSalaryConverter()
    {
//    	TaxCalculator tc = new TaxCalculator() ;
//    	
//    	int[] chosenPaymentPeriod = {PaymentPeriod.DAILY,PaymentPeriod.WEEKLY,PaymentPeriod.FORTNIGHTLY,PaymentPeriod.MONTHLY,PaymentPeriod.YEARLY} ;// daily, weekly , monthly , yearly
//    	double remuneration = 3000 ;
//    	double[] expectedSalary = {1095000, } ;
//    	
//    	double[] actualSalary = new double[5] ;
//    	for ( int i = 0 ; i < 5 ; ++i )
//    		actualSalary[i] = tc.convertToSalary(chosenPaymentPeriod, remuneration[chosenPaymentPeriod]) ;
//    	
//    	for ( int i = 0 ; i < 5 ; ++i )
//    		assertEquals(actualSalary[i],expectedSalary[i]) ;
    	assertTrue(true) ;
    }
    
    public void testAllowableBenefits()
    {
    	//benefits above percentage of salary allowed and above percentage of salary threshold
    	double benefits25perc = 550000.0 ;
    	double salary = 2000000.0 ;
    	assertEquals(TaxMacros.MaxAllowableBenefits,TaxCalculator.getAllowableBenefits(benefits25perc,salary)) ;
    	
    	//benefits below maximum allowable amount and below percentage of salary allowed to go towards tax benefits
    	benefits25perc = 247500 ;
    	salary = 1000000 ;
    	assertEquals(benefits25perc,TaxCalculator.getAllowableBenefits(benefits25perc, salary)) ;
    	
    	//benefits below maximum tax benefits threshold but above % salary threshold
    	double benefits = 300000.0 ;
    	salary = 1000000.0 ;
    	assertEquals(TaxMacros.PercOfSalaryAllowableAsBenefits*salary,TaxCalculator.getAllowableBenefits(benefits, salary)) ;
    }
    
    public void testTaxableIncomeCalculator()
    {
    	double salary = 500000 ;
    	double benefits = 100000 ;
    	//interest above tax threshold
    	double interest = 123892 ;
    	
    	double expectedTI = salary - benefits + (interest-TaxMacros.InterestExemption) ;
    	
    	assertEquals(expectedTI , TaxCalculator.getTaxableIncome(salary, benefits, interest)) ;
    	
    	//interest below exemption threshold
    	interest = 13892 ;
    	expectedTI = salary - benefits + interest ;
    	
    	assertEquals(expectedTI , TaxCalculator.getTaxableIncome(salary, benefits, interest)) ;
    }
    
    public void testTaxPayableCalculator()
    {
    	//the first tax bracket
    	double taxableIncome = 10000 ;
    	double expectedTP = 0 ;
    	
    	assertEquals(expectedTP,TaxCalculator.getTaxPayable(taxableIncome)) ;
    	
    	//second tax bracket
    	taxableIncome = 273500 ;
    	expectedTP = 35253 + .26 * (taxableIncome-195851) ;
    	
    	assertEquals(expectedTP,TaxCalculator.getTaxPayable(taxableIncome)) ;
    	
    	//third tax bracket
    	taxableIncome = 402500 ;
    	expectedTP = 63853 + .31*(taxableIncome-305851) ;
    	
    	assertEquals(expectedTP,TaxCalculator.getTaxPayable(taxableIncome)) ;
    }

    public void testFinalTaxOwed()
    {
    	//tax payable less than rebate amount
    	double taxPayable = 4000 ;
    	double expectedFTO = 0 ;
    	
    	assertEquals( expectedFTO , TaxCalculator.getFinalTaxOwed(taxPayable,14220.0) ) ;
    	
    	//tax payable more than rebate amount
    	taxPayable = 70000 ;
    	expectedFTO = taxPayable-14220.0 ;
    	
    	assertEquals( expectedFTO , TaxCalculator.getFinalTaxOwed(taxPayable,14220.0) ) ;
    }
    
    public void testGetRebateAmount()
    {
    	//for under 65
    	assertEquals(14220.0,TaxCalculator.getRebate(60)) ;
    	
    	//for 65-75
    	assertEquals(14220.0+7794.0,TaxCalculator.getRebate(65)) ;
    	assertEquals(14220.0+7794.0,TaxCalculator.getRebate(75)) ;
    	
    	//for above 75
    	assertEquals(14220.0+7794.0+2601.0,TaxCalculator.getRebate(80)) ;
    }
}