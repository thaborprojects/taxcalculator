package com.psybergate.vac201912.taxHandling;

public class PaymentPeriod
{
	public static final int DAILY = 0 ;
	public static final int WEEKLY = 1 ;
	public static final int FORTNIGHTLY = 2 ;
	public static final int MONTHLY = 3 ;
	public static final int YEARLY = 4 ;
}