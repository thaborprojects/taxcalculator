package com.psybergate.vac201912.uiCoordinationServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.psybergate.vac201912.dbHandling.DatabaseHandler;
import com.psybergate.vac201912.taxHandling.TaxPayer;

public class AddNewTaxPayer extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public void doPost( HttpServletRequest req , HttpServletResponse res ) throws ServletException, IOException
	{
		//we create a tax payer object
		//TODO add new taxPayer to DB then route to getTaxRecord info
		int taxNumber = Integer.parseInt(req.getParameter("taxNumber")) ;
		String name = req.getParameter("name") ;
		String surname = req.getParameter("surname") ;
		int yearOfBirth = Integer.parseInt(req.getParameter("yearOfBirth")) ;
		TaxPayer taxPayer = new TaxPayer(taxNumber,name,surname,yearOfBirth) ;
		
		//adding new taxPayer to db
		try
		{
		DatabaseHandler.addTaxPayer( taxPayer ) ;
		
		//pass the taxPayer object to the session
		HttpSession session = req.getSession() ;
		session.setAttribute("taxPayer", taxPayer);
		
		//TODO display logout option or add new tax record
		//FIXME for now routing to add new tax record
		//we do db query as user enters tax number, this will need tag
		RequestDispatcher rd = req.getRequestDispatcher("GetTaxRecordInfo.jsp") ;
		rd.forward(req, res);
		}catch( SQLException e )
		{
			RequestDispatcher rd = req.getRequestDispatcher("errorPage.jsp") ;
			req.setAttribute("error", e);
			e.printStackTrace();
			rd.forward(req, res);
		}
	}
	
}
