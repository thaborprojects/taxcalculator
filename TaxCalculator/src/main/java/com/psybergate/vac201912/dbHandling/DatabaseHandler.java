package com.psybergate.vac201912.dbHandling ;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

import com.psybergate.vac201912.taxHandling.TaxRecord;
import com.psybergate.vac201912.taxHandling.TaxPayer;

public class DatabaseHandler
{
	private static Connection databaseConnection ;
	
	public static void connectToDatabase(String databaseName ) throws SQLException
	{
		//database name and password need to change per system
		databaseConnection = DriverManager.getConnection(String.format("jdbc:postgresql://localhost:5432/%s",databaseName),"postgres","postgrespw") ;
	}
	
	public static void disconnectFromDB() throws SQLException
	{
		databaseConnection.close();
	}
	
	public static void onFirstLaunch() throws SQLException
	{
		//method defines what happens when application is run for the first time on the system
		//requires creation of database "javaprojectsdb"
		
		//connect to db
		connectToDatabase("javaprojectsdb") ;
		
		//create tax payer table
		//taxpayer_tax_number ; taxpayer_name ; taxpayer_surname ; taxpayer_yob
		String query = "CREATE TABLE IF NOT EXISTS TAXPAYER("
				+ "taxpayer_tax_number INT PRIMARY KEY NOT NULL,"
				+ "taxpayer_name VARCHAR,"
				+ "taxpayer_surname VARCHAR,"
				+ "taxpayer_yob INT NOT NULL"
				+ ")" ;
		PreparedStatement statement = databaseConnection.prepareStatement(query) ;
		statement.execute() ;
		
		//create tax record table
		//taxrecord_tax_number ; taxrecord_taxpayer_age ; taxrecord_year ; taxrecord_salary ; taxrecord_benefits ; taxrecord_interest
		query = "CREATE TABLE IF NOT EXISTS TAXRECORD("
				+ "taxrecord_tax_number INT NOT NULL,"
				+ "taxrecord_taxpayer_age INT NOT NULL,"
				+ "taxrecord_year INT NOT NULL,"
				+ "taxrecord_salary NUMERIC DEFAULT 0,"
				+ "taxrecord_benefits NUMERIC DEFAULT 0,"
				+ "taxrecord_interest NUMERIC DEFAULT 0,"
				+ "PRIMARY KEY(taxrecord_tax_number,taxrecord_year)"
				+ ")";
		statement = databaseConnection.prepareStatement(query) ;
		statement.execute() ;
		
		//close db connection
		disconnectFromDB();
	}
	
	public static Map<Integer, TaxRecord> getAllTaxRecords( int taxNumber ) throws SQLException
	{
		connectToDatabase("javaprojectsdb");
		String query = "SELECT * FROM TAX_RECORD WHERE TAX_RECORD_TAX_NUMBER = ? " ;
		PreparedStatement statement = databaseConnection.prepareStatement(query) ;
		statement.setInt(1, taxNumber);
		
		ResultSet results = statement.executeQuery() ;
		
		Map<Integer, TaxRecord> allRecords = new HashMap<Integer, TaxRecord>() ;
		
		while(results.next() )
		{
			int year = results.getInt("tax_record_year") ;
			int age = results.getInt("tax_record_tax_payer_age") ;
			double salary = results.getDouble("tax_record_salary") ;
			double interest = results.getDouble("tax_record_interest") ;
			double benefits = results.getDouble("tax_record_benefits") ;
			
			allRecords.put(year,new TaxRecord(taxNumber,age,year,salary,interest,benefits)) ;
		}
		disconnectFromDB();
		return allRecords ;
	}
	public static void addTaxPayer( TaxPayer taxPayer ) throws SQLException
	{
		connectToDatabase("javaprojectsdb");
		
		String query = "INSERT INTO TAX_PAYER(TAX_PAYER_NAME,TAX_PAYER_SURNAME,TAX_PAYER_TAX_NUMBER,TAX_PAYER_YOB) VALUES(?,?,?,?)" ;
		
		PreparedStatement statement = databaseConnection.prepareStatement(query) ;
		statement.setString(1, taxPayer.getName());
		statement.setString(2, taxPayer.getSurname());
		statement.setInt(3,taxPayer.getTaxNumber()) ;
		statement.setInt(4, taxPayer.getYearOfBirth());
		
		statement.executeUpdate() ;
		
		disconnectFromDB();
	}
	
	public static boolean taxPayerExists( int taxNumber ) throws SQLException
	{
		connectToDatabase("javaprojectsdb");

		String query = "SELECT TAX_PAYER_TAX_NUMBER FROM TAX_PAYER WHERE TAX_PAYER_TAX_NUMBER = ?" ;
		PreparedStatement statement = databaseConnection.prepareStatement(query) ;
		statement.setInt(1, taxNumber);
		
		ResultSet results = statement.executeQuery() ;
		
		boolean exists = !results.isLast() ;
		disconnectFromDB();
		
		return exists ;
	}

	public static void addTaxRecord(TaxRecord taxRecord) throws SQLException
	{
		connectToDatabase("javaprojectsdb");
		
		String query = "INSERT INTO TAX_RECORD(TAX_RECORD_YEAR,TAX_RECORD_TAX_NUMBER,TAX_RECORD_SALARY,TAX_RECORD_INTEREST,TAX_RECORD_BENEFITS,TAX_RECORD_TAX_PAYER_AGE) VALUES(?,?,?,?,?,?)" ;
		PreparedStatement statement = databaseConnection.prepareStatement(query) ;
		statement.setInt(1, taxRecord.getYear());
		statement.setInt(2, taxRecord.getTaxNumber());
		statement.setDouble(3, taxRecord.getSalary());
		statement.setDouble(4, taxRecord.getInterest());
		statement.setDouble(5, taxRecord.getBenefits());
		statement.setInt(6, taxRecord.getAge());
		
		statement.executeUpdate() ;
		disconnectFromDB();
	}

	public static TaxPayer getTaxPayer(int taxNumber) throws SQLException
	{
		connectToDatabase("javaprojectsdb");

		String query = "SELECT * FROM TAX_PAYER WHERE TAX_PAYER_TAX_NUMBER = ?" ;
		PreparedStatement statement = databaseConnection.prepareStatement(query) ;
		statement.setInt(1, taxNumber);
		
		ResultSet results = statement.executeQuery() ;
		results.next() ;
		TaxPayer taxPayer = null ;
		
		if ( !results.isAfterLast() )
		{
			//tax payer exists
			String name = results.getString("TAX_PAYER_NAME") ;
			String surname = results.getString("TAX_PAYER_SURNAME") ;
			int yearOfBirth = results.getInt("TAX_PAYER_YOB") ;
			taxPayer = new TaxPayer(taxNumber,name,surname,yearOfBirth) ;
		}
		
		disconnectFromDB();
		return taxPayer ;
	}
}
