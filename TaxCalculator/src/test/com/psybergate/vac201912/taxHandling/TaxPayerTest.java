package com.psybergate.vac201912.taxHandling ;
import static org.junit.Assert.*;

import org.junit.*;

public class TaxPayerTest
{
	TaxPayer taxPayer ;
	@Before
	public void setUp()
	{
		//instantiating a TaxPayer object
		taxPayer = new TaxPayer( 123456 , "Tax","Payer", 1975 ) ;
	}
	
	@After
	public void tearDown()
	{
		//destroying TaxPayer object,not necessary
		taxPayer = null ;
	}
	
	@Test
	public void getTaxNumber()
	{
		assertEquals(123456,taxPayer.getTaxNumber()) ;
	}
	
	@Test
	public void getAgeTest()
	{
		assertEquals(44,taxPayer.getAge()) ;
	}
	
	@Test
	public void getNameTest()
	{
		assertEquals("Tax",taxPayer.getName()) ;
	}
	
	@Test
	public void getSurnameTest()
	{
		assertEquals("Payer",taxPayer.getSurname()) ;
	}

	@Test
	public void getTaxRecordTest()
	{
		//returns null for an invalid year or unavailable year
		assertNull(taxPayer.getTaxRecord(0)) ;
		
		//returns a none-null tax record of taxNumber == 123456
		taxPayer.addTaxRecord(2019, 500000, 150000, 70000) ;
		assertNotNull(taxPayer.getTaxRecord(2019)) ;
		assertEquals(taxPayer.getTaxNumber(),taxPayer.getTaxRecord(2019).getTaxNumber()) ;
	}
	
	@Test
	public void getAllTaxRecordsTest()
	{
		//ensures none null collection returned
		assertNotNull(taxPayer.getAllTaxRecords()) ;
		
		//ensures returns correctly sized taxRecords list
		taxPayer.addTaxRecord(0, 0, 0, 0);
		taxPayer.addTaxRecord(1, 0, 0, 0);
		assertEquals(2,taxPayer.getAllTaxRecords().size()) ;
	}
	
	@Test
	public void taxStatusTest()
	{
		taxPayer.addTaxRecord(1999, 0, 0, 0);
		
		//testing for unavailable old and future years
		assertEquals(TaxStatus.NOT_AVAILABLE,taxPayer.taxStatus(1966)) ;
		assertEquals(TaxStatus.NOT_AVAILABLE,taxPayer.taxStatus(2121)) ;
		
		//testing record for current year not yet calculated
		assertEquals(TaxStatus.PENDING,taxPayer.taxStatus(2019)) ;
		
		//testing for available record
		assertEquals(TaxStatus.CALCULATED,taxPayer.taxStatus(1999)) ;
	}

	//TODO write tests for setters
}
